package com.brighttalk.realm.bttest.util;

import com.brighttalk.realm.bttest.dto.UserRequestDto;
import com.brighttalk.realm.bttest.dto.UserResponseDto;
import com.brighttalk.realm.bttest.entity.User;

public final class UserConverter {

	public static User convertUserRequestDtoToUser(UserRequestDto userRequestDto) {
		User user = new User();
		user.setName(userRequestDto.getName());
		user.setDescription(userRequestDto.getDescription());
		return user;
	}

	public static UserResponseDto convertUserToUserResponseDto(User user) {
		UserResponseDto userResponseDto = new UserResponseDto();
		userResponseDto.setDescription(user.getDescription());
		userResponseDto.setName(user.getName());
		userResponseDto.setId(user.getId());
		userResponseDto.setKey(user.getKey());
		return userResponseDto;
	}
}
