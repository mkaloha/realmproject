package com.brighttalk.realm.bttest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BttestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BttestApplication.class, args);
	}

}
