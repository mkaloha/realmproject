package com.brighttalk.realm.bttest.controller;

import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.brighttalk.realm.bttest.dto.UserRequestDto;
import com.brighttalk.realm.bttest.dto.UserResponseDto;
import com.brighttalk.realm.bttest.entity.User;
import com.brighttalk.realm.bttest.exception.InvalidUserNameException;
import com.brighttalk.realm.bttest.service.UserService;
import com.brighttalk.realm.bttest.util.UserConverter;

@RestController
@RequestMapping(path = "/service/user")
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public UserResponseDto getUserInfo(@PathVariable("id") Integer id) {

		User user = userService.getUser(id);
		return UserConverter.convertUserToUserResponseDto(user);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<UserResponseDto> addUserInfo(@RequestBody UserRequestDto realmRequestDto)
			throws NoSuchAlgorithmException {
		validateRequest(realmRequestDto);
		User user = userService.addUser(UserConverter.convertUserRequestDtoToUser(realmRequestDto));
		return new ResponseEntity<UserResponseDto>(UserConverter.convertUserToUserResponseDto(user),
				HttpStatus.CREATED);
	}

	private void validateRequest(UserRequestDto realmRequestDto) {
		if (null == realmRequestDto.getName() || realmRequestDto.getName().isEmpty()) {
			throw new InvalidUserNameException();
		}
	}
}
