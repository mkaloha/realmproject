package com.brighttalk.realm.bttest.exception;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@XmlRootElement(name = "error")
@JsonInclude(value = Include.NON_NULL)
public class ErrorDto {
	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ErrorDto() {
		super();
	}

}
