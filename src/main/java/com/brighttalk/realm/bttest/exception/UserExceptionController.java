package com.brighttalk.realm.bttest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class UserExceptionController {


	@ExceptionHandler(value = NumberFormatException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody ErrorDto invalidUserException(NumberFormatException exception) {
		ErrorDto errorDto = new ErrorDto();
		errorDto.setCode("InvalidArgument");
		return errorDto;
	}
	
	@ExceptionHandler(value = InvalidUserNameException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody ErrorDto invalidRealmNameException(InvalidUserNameException exception) {
		ErrorDto errorDto = new ErrorDto();
		errorDto.setCode("InvalidUserName");
		return errorDto;
	}
	
	@ExceptionHandler(value = DuplicateNameException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody ErrorDto duplicateRealmNameException(DuplicateNameException exception) {
		ErrorDto errorDto = new ErrorDto();
		errorDto.setCode("DuplicateUserName");
		return errorDto;
	}
	
}
