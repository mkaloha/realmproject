package com.brighttalk.realm.bttest.service;

import java.security.NoSuchAlgorithmException;

import com.brighttalk.realm.bttest.entity.User;

public interface UserService {
	 public User getUser(Integer id);	 
	 public User addUser(User userRequest) throws NoSuchAlgorithmException;	 
}
