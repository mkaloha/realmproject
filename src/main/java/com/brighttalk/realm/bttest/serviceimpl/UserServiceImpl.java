package com.brighttalk.realm.bttest.serviceimpl;

import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Optional;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brighttalk.realm.bttest.dao.UserRepository;
import com.brighttalk.realm.bttest.entity.User;
import com.brighttalk.realm.bttest.exception.DuplicateNameException;
import com.brighttalk.realm.bttest.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Override
	public User getUser(Integer id) {
		Optional<User> realm = userRepository.findById(id);
		return realm.get();
	}

	@Override
	public User addUser(User userRequest) throws NoSuchAlgorithmException {
		KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
		SecretKey secretKey = keyGenerator.generateKey();
		String key = Base64.getEncoder().encodeToString(secretKey.getEncoded());

		userRequest.setKey(key);
		try {
			User user = userRepository.save(userRequest);
			return user;
		} catch (Exception e) {
			throw new DuplicateNameException();
		}
	}

}
