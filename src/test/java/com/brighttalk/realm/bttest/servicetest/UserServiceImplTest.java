package com.brighttalk.realm.bttest.servicetest;

import static org.assertj.core.api.Assertions.assertThat;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.brighttalk.realm.bttest.dao.UserRepository;
import com.brighttalk.realm.bttest.dto.UserRequestDto;
import com.brighttalk.realm.bttest.entity.User;
import com.brighttalk.realm.bttest.service.UserService;
import com.brighttalk.realm.bttest.serviceimpl.UserServiceImpl;
import com.brighttalk.realm.bttest.testutil.DataUtil;
import com.brighttalk.realm.bttest.util.UserConverter;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { UserServiceImpl.class })
public class UserServiceImplTest {

	@Autowired
	UserService userService;

	@MockBean
	UserRepository userRepository;

	User user;
	UserRequestDto userRequestDto;

	@Before
	public void initializeUser() {
		user = DataUtil.generateUser();
		userRequestDto = DataUtil.generateUseRequest();
	}

	@Test
	public void testGetUser() {
		Optional<User> userOptional = Optional.of(user);
		Mockito.when(userRepository.findById(Mockito.anyInt())).thenReturn(userOptional);

		User user = userService.getUser(1);

		assertThat(user).isNotNull();
		assertThat(user.getId()).isEqualTo(1);
	}

	@Test(expected = NullPointerException.class)
	public void testGetUserInvalidId() {
		Optional<User> userOptional = Optional.of(null);
		Mockito.when(userRepository.findById(Mockito.anyInt())).thenReturn(userOptional);
		userService.getUser(0);
	}

	@Test(expected = NullPointerException.class)
	public void testGetUserNullValue() {
		Optional<User> userOptional = Optional.of(null);
		Mockito.when(userRepository.findById(Mockito.anyInt())).thenReturn(userOptional);
		userService.getUser(null);
	}

	@Test
	public void testAddUser() throws NoSuchAlgorithmException {
		Mockito.when(userRepository.save(Mockito.any())).thenReturn(user);
		User newUser = UserConverter.convertUserRequestDtoToUser(userRequestDto);

		User user = userService.addUser(newUser);

		assertThat(user).isNotNull();
		assertThat(user.getId()).isEqualTo(1);
		assertThat(user.getName()).isEqualTo(newUser.getName());
	}
}
