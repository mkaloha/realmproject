package com.brighttalk.realm.bttest.controllertest;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.brighttalk.realm.bttest.dto.UserRequestDto;
import com.brighttalk.realm.bttest.dto.UserResponseDto;
import com.brighttalk.realm.bttest.exception.DuplicateNameException;
import com.brighttalk.realm.bttest.exception.ErrorDto;
import com.brighttalk.realm.bttest.service.UserService;
import com.brighttalk.realm.bttest.testutil.DataUtil;

public class UserControllerTest extends AbstractTest {

	@MockBean
	UserService userService;

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void getUsers() throws Exception {
		String uri = "/service/user/{id}";
		Mockito.when(userService.getUser(Mockito.anyInt())).thenReturn(DataUtil.generateUser());
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri, 1).accept(MediaType.APPLICATION_JSON))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		UserResponseDto userResponseDto = super.mapFromJson(content, UserResponseDto.class);
		assertTrue(userResponseDto.getName().equals("name"));
	}
	
	@Test
	public void getUsersInvalidId() throws Exception {
		String uri = "/service/user/{id}";
		Mockito.when(userService.getUser(Mockito.anyInt())).thenThrow(NumberFormatException.class);
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri, 0).accept(MediaType.APPLICATION_JSON))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(400, status);
		String content = mvcResult.getResponse().getContentAsString();
		ErrorDto errorDto = super.mapFromJson(content, ErrorDto.class);
		assertTrue(errorDto.getCode().equals("InvalidArgument"));
	}

	@Test
	public void addUser() throws Exception {
		String uri = "/service/user";
		Mockito.when(userService.addUser(Mockito.any())).thenReturn(DataUtil.generateUser());

		String inputJson = super.mapToJson(DataUtil.generateUseRequest());
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(201, status);
		String content = mvcResult.getResponse().getContentAsString();
		UserResponseDto userResponseDto = super.mapFromJson(content, UserResponseDto.class);
		assertTrue(userResponseDto.getName().equals("name"));
	}

	@Test
	public void addUserInvalidObject() throws Exception {
		String uri = "/service/user";
		Mockito.when(userService.addUser(Mockito.any())).thenReturn(DataUtil.generateUser());
		UserRequestDto userRequestDto = DataUtil.generateUseRequest();
		userRequestDto.setName("");
		String inputJson = super.mapToJson(userRequestDto);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(400, status);
		String content = mvcResult.getResponse().getContentAsString();
		ErrorDto errorDto = super.mapFromJson(content, ErrorDto.class);
		assertTrue(errorDto.getCode().equals("InvalidUserName"));
	}
	
	@Test
	public void addUserDuplicateName() throws Exception {
		String uri = "/service/user";
		Mockito.when(userService.addUser(Mockito.any())).thenThrow(DuplicateNameException.class);

		String inputJson = super.mapToJson(DataUtil.generateUseRequest());
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(400, status);
		String content = mvcResult.getResponse().getContentAsString();
		ErrorDto errorDto = super.mapFromJson(content, ErrorDto.class);
		assertTrue(errorDto.getCode().equals("DuplicateUserName"));
	}
}
