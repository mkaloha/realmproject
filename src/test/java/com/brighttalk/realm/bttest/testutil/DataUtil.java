package com.brighttalk.realm.bttest.testutil;

import com.brighttalk.realm.bttest.dto.UserRequestDto;
import com.brighttalk.realm.bttest.entity.User;

public final class DataUtil {

	public static User generateUser() {
		User user = new User();
		user.setDescription("description");
		user.setName("name");
		user.setId(1);
		user.setKey("key");
		return user;
	}
	
	public static UserRequestDto generateUseRequest() {
		UserRequestDto userRequestDto = new UserRequestDto();
		userRequestDto.setDescription("description");
		userRequestDto.setName("name");
		return userRequestDto;
	}
}
