# User Info

User info is data handling restful service.

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Gradle](https://gradle.org/)

## Installation

Use the package manager [gradle](https://gradle.org/) to install dependancies.

## Running the application locally
There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.brighttalk.realm.bttest.BttestApplication` class from your IDE.
Alternatively you can use below command as well to build project.
```bash
gradle clean build
```
### Database schema
Create new schema using script.

```
CREATE DATABASE `realmDb`
```

Create table realm inside realmdb.

```
CREATE TABLE `realmDb`.`realm` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(255) NULL,
  `r_key` VARCHAR(32) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC));
```

### Running the tests
Explain how to run the automated tests for this system
```bash
gradle clean test
```

